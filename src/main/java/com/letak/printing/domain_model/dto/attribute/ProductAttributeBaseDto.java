package com.letak.printing.domain_model.dto.attribute;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.letak.printing.domain_model.dto.product.ProductBaseDto;
import com.letak.printing.domain_model.entity.ProductAttribute;

public class ProductAttributeBaseDto {

    private Integer id;

    private String name;

    private String scriptName;

    private String type;

    private Integer displayOrder;

    @JsonProperty(access = Access.WRITE_ONLY)
    private ProductBaseDto product;

    public ProductAttributeBaseDto() {}

    public ProductAttributeBaseDto(ProductAttribute pa) {
        setId(pa.getId());
        setName(pa.getName());
        setScriptName(pa.getScriptName());
        setType(pa.getType());
        setDisplayOrder(pa.getDisplayOrder());
        
//        setProduct(new ProductBaseDto(pa.getProduct()));
    }

    public static ProductAttribute createEntity(ProductAttributeBaseDto dto) {
        ProductAttribute entity = new ProductAttribute();
        
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setScriptName(dto.getScriptName());
        entity.setType(dto.getType());
        entity.setDisplayOrder(dto.getDisplayOrder());
        
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public ProductBaseDto getProduct() {
        return product;
    }

    public void setProduct(ProductBaseDto product) {
        this.product = product;
    }
}
