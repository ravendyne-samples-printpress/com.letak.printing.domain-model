package com.letak.printing.domain_model.dto.product;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.dto.parameter.ProductParameterWithChildrenDto;
import com.letak.printing.domain_model.dto.script.QuoteScriptBaseDto;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductParameter;
import com.letak.printing.domain_model.entity.QuoteScript;

public class ProductWithChildrenDto extends ProductWithAttributesDto {

    private List<ProductParameterWithChildrenDto> productParameters;

    private List<QuoteScriptBaseDto> productScripts;
    
    public ProductWithChildrenDto() {}
    
    public ProductWithChildrenDto(Product product) {
        super(product);
        
        productParameters = new ArrayList<>();
        productScripts = new ArrayList<>();
        
        for(ProductParameter param : product.getProductParameters()) {
            productParameters.add(new ProductParameterWithChildrenDto(param));
        }

        for(QuoteScript script : product.getProductScripts()) {
            productScripts.add(new QuoteScriptBaseDto(script));
        }
    }
    
    public static Product createEntity(ProductWithChildrenDto dto) {
        Product entity = ProductBaseDto.createEntity(dto);
        
        List<ProductParameter> productParametersList = new ArrayList<>();
        
        for(ProductParameterWithChildrenDto pp : dto.getProductParameters()) {
            productParametersList.add(ProductParameterWithChildrenDto.createEntity(pp));
        }

        entity.setProductParameters(productParametersList);
        
        List<QuoteScript> productScriptsList = new ArrayList<>();
        
        for(QuoteScriptBaseDto qs : dto.getProductScripts()) {
            productScriptsList.add(QuoteScriptBaseDto.createEntity(qs));
        }
        
        entity.setProductScripts(productScriptsList);
        
        return entity;
    }

    public List<ProductParameterWithChildrenDto> getProductParameters() {
        return productParameters;
    }

    public void setProductParameters(List<ProductParameterWithChildrenDto> productParameters) {
        this.productParameters = productParameters;
    }

    public List<QuoteScriptBaseDto> getProductScripts() {
        return productScripts;
    }

    public void setProductScripts(List<QuoteScriptBaseDto> productScripts) {
        this.productScripts = productScripts;
    }
}
