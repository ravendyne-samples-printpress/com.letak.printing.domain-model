package com.letak.printing.domain_model.dto.product;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.dto.parameter.ProductParameterBaseDto;
import com.letak.printing.domain_model.dto.script.QuoteScriptBaseDto;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductParameter;
import com.letak.printing.domain_model.entity.QuoteScript;

public class ProductWithBaseChildrenDto extends ProductWithBaseAttributesDto {

    private List<ProductParameterBaseDto> productParameters;

    private List<QuoteScriptBaseDto> productScripts;
    
    public ProductWithBaseChildrenDto() {}
    
    public ProductWithBaseChildrenDto(Product product) {
        super(product);
        
        productParameters = new ArrayList<>();
        productScripts = new ArrayList<>();
        
        for(ProductParameter param : product.getProductParameters()) {
            productParameters.add(new ProductParameterBaseDto(param));
        }

        for(QuoteScript script : product.getProductScripts()) {
            productScripts.add(new QuoteScriptBaseDto(script));
        }
    }
    
    public static Product createEntity(ProductWithBaseChildrenDto dto) {
        Product entity = ProductBaseDto.createEntity(dto);
        
        List<ProductParameter> productParametersList = new ArrayList<>();

        for(ProductParameterBaseDto pp : dto.getProductParameters()) {
            productParametersList.add(ProductParameterBaseDto.createEntity(pp));
        }

        entity.setProductParameters(productParametersList);
        
        List<QuoteScript> productScriptsList = new ArrayList<>();
        
        for(QuoteScriptBaseDto qs : dto.getProductScripts()) {
            productScriptsList.add(QuoteScriptBaseDto.createEntity(qs));
        }
        
        entity.setProductScripts(productScriptsList);
        
        return entity;
    }

    public List<ProductParameterBaseDto> getProductParameters() {
        return productParameters;
    }

    public void setProductParameters(List<ProductParameterBaseDto> productParameters) {
        this.productParameters = productParameters;
    }

    public List<QuoteScriptBaseDto> getProductScripts() {
        return productScripts;
    }

    public void setProductScripts(List<QuoteScriptBaseDto> productScripts) {
        this.productScripts = productScripts;
    }
}
