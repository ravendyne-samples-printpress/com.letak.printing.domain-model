package com.letak.printing.domain_model.dto.product;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.dto.attribute.ProductAttributeBaseDto;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductAttribute;

public class ProductWithBaseAttributesDto extends ProductBaseDto {

    private List<ProductAttributeBaseDto> productAttributes;
    
    public ProductWithBaseAttributesDto() {}
    
    public ProductWithBaseAttributesDto(Product product) {
        super(product);
        
        productAttributes = new ArrayList<>();
        for(ProductAttribute attr : product.getProductAttributes()){
            productAttributes.add(new ProductAttributeBaseDto(attr));
        }
    }
    
    public static Product createEntity(ProductWithBaseAttributesDto dto) {
        Product entity = ProductBaseDto.createEntity(dto);
        
        List<ProductAttribute> productAttributesList = new ArrayList<>();
        
        for(ProductAttributeBaseDto attr : dto.getProductAttributes()) {
            productAttributesList.add(ProductAttributeBaseDto.createEntity(attr));
        }
        
        entity.setProductAttributes(productAttributesList);
        
        return entity;
    }

    public List<ProductAttributeBaseDto> getProductAttributes() {
        return productAttributes;
    }

    public void setProductAttributes(List<ProductAttributeBaseDto> productAttributes) {
        this.productAttributes = productAttributes;
    }
}
