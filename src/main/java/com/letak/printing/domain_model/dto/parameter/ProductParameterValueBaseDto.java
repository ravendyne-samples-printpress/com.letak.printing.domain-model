package com.letak.printing.domain_model.dto.parameter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.letak.printing.domain_model.entity.ProductParameterValue;

public class ProductParameterValueBaseDto {

    private Integer id;

    private String value;

    private String scriptValue;

    private Integer displayOrder;

    @JsonProperty(access = Access.WRITE_ONLY)
    private ProductParameterBaseDto productParameter;

    public ProductParameterValueBaseDto() {
    }

    public ProductParameterValueBaseDto(ProductParameterValue productParameterValue) {
        setId(productParameterValue.getId());
        setValue(productParameterValue.getValue());
        setScriptValue(productParameterValue.getScriptValue());
        setDisplayOrder(productParameterValue.getDisplayOrder());
        
//        setProductParameter(new ProductParameterBaseDto(productParameterValue.getProductParameter()));
    }
    
    public static ProductParameterValue createEntity(ProductParameterValueBaseDto dto) {
        ProductParameterValue entity = new ProductParameterValue();
        
        entity.setId(dto.getId());
        entity.setValue(dto.getValue());
        entity.setScriptValue(dto.getScriptValue());
        entity.setDisplayOrder(dto.getDisplayOrder());
        
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScriptValue() {
        return scriptValue;
    }

    public void setScriptValue(String scriptValue) {
        this.scriptValue = scriptValue;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public ProductParameterBaseDto getProductParameter() {
        return productParameter;
    }

    public void setProductParameter(ProductParameterBaseDto productParameter) {
        this.productParameter = productParameter;
    }
}
