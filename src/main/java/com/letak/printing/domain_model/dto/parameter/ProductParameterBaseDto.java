package com.letak.printing.domain_model.dto.parameter;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.letak.printing.domain_model.dto.product.ProductBaseDto;
import com.letak.printing.domain_model.entity.ProductParameter;

public class ProductParameterBaseDto {

    private Integer id;

    private String name;

    private String scriptName;

    private String defaultValue;

    private String type;

    @JsonProperty(access = Access.WRITE_ONLY)
    private ProductBaseDto product;
    
    public ProductParameterBaseDto() {
    }
    
    public ProductParameterBaseDto(ProductParameter productParameter) {
        setId(productParameter.getId());
        setName(productParameter.getName());
        setScriptName(productParameter.getScriptName());
        setDefaultValue(productParameter.getDefaultValue());
        setType(productParameter.getType());
        
//        setProduct(new ProductBaseDto(productParameter.getProduct()));
    }
    
    public static ProductParameter createEntity(ProductParameterBaseDto dto) {
        ProductParameter entity = new ProductParameter();
        
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setScriptName(dto.getScriptName());
        entity.setDefaultValue(dto.getDefaultValue());
        entity.setType(dto.getType());
        
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ProductBaseDto getProduct() {
        return product;
    }

    public void setProduct(ProductBaseDto product) {
        this.product = product;
    }

}
