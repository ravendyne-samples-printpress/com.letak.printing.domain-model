package com.letak.printing.domain_model.dto.attribute;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.letak.printing.domain_model.entity.ProductAttributeValue;

public class ProductAttributeValueBaseDto {

    private Integer id;

    private String value;

    private String scriptValue;

    private Integer displayOrder;

    @JsonProperty(access = Access.WRITE_ONLY)
    private ProductAttributeBaseDto productAttribute;

    public ProductAttributeValueBaseDto() {}

    public ProductAttributeValueBaseDto(ProductAttributeValue pav) {
        setId(pav.getId());
        setValue(pav.getValue());
        setScriptValue(pav.getScriptValue());
        setDisplayOrder(pav.getDisplayOrder());
        
//        setProductAttribute(new ProductAttributeBaseDto(pav.getProductAttribute()));
    }
    
    public static ProductAttributeValue createEntity(ProductAttributeValueBaseDto dto) {
        ProductAttributeValue entity = new ProductAttributeValue();
        
        entity.setId(dto.getId());
        entity.setValue(dto.getValue());
        entity.setScriptValue(dto.getScriptValue());
        entity.setDisplayOrder(dto.getDisplayOrder());

        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getScriptValue() {
        return scriptValue;
    }

    public void setScriptValue(String scriptValue) {
        this.scriptValue = scriptValue;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public ProductAttributeBaseDto getProductAttribute() {
        return productAttribute;
    }

    public void setProductAttribute(ProductAttributeBaseDto productAttribute) {
        this.productAttribute = productAttribute;
    }
}
