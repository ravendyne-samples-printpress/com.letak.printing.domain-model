package com.letak.printing.domain_model.dto.printpress;

import com.letak.printing.domain_model.entity.PrintPress;

public class PrintPressBaseDto {

    private Integer id;

    private String name;

    private String uuid;

    private String email;

    public PrintPressBaseDto() {
    }

    public PrintPressBaseDto(PrintPress printPress) {
        setId(printPress.getId());
        setName(printPress.getName());
        setUuid(printPress.getUuid());
        setEmail(printPress.getEmail());
    }
    
    public static PrintPress createEntity(PrintPressBaseDto dto) {
        PrintPress entity = new PrintPress();
        
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setUuid(dto.getUuid());
        entity.setEmail(dto.getEmail());

        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
