package com.letak.printing.domain_model.dto.order;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.letak.printing.domain_model.dto.printpress.PrintPressBaseDto;
import com.letak.printing.domain_model.entity.PrintPressOrder;

public class PrintPressOrderBaseDto {

    private Integer id;

    private String name;

    private String address;

    private String phone;

    private String company;

    private String email;

    private String note;

    private String orderNumber;

    @JsonProperty(access = Access.WRITE_ONLY)
    private PrintPressBaseDto printPress;
    
    public PrintPressOrderBaseDto() {}
    
    public PrintPressOrderBaseDto(PrintPressOrder order) {
        setId(order.getId());
        setName(order.getName());
        setAddress(order.getAddress());
        setPhone(order.getPhone());
        setCompany(order.getCompany());
        setEmail(order.getEmail());
        setOrderNumber(order.getOrderNumber());
        setNote(order.getNote());
        
//        setPrintPress(new PrintPressBaseDto(order.getPrintPress()));
    }
    
    public static PrintPressOrder createEntity(PrintPressOrderBaseDto dto) {
        PrintPressOrder entity = new PrintPressOrder();
        
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
        entity.setPhone(dto.getPhone());
        entity.setCompany(dto.getCompany());
        entity.setEmail(dto.getEmail());
        entity.setOrderNumber(dto.getOrderNumber());
        entity.setNote(dto.getNote());
        
        return entity;
    }
    
    public static PrintPressOrderBaseDto createDto(Map<String, String> data) throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        PrintPressOrderBaseDto dto = mapper.convertValue(data, PrintPressOrderBaseDto.class);
        PrintPressOrderBaseDto.createEntity(dto);

        return dto;
    }
    
    @JsonAnySetter
    public void catchAny(String name, String value) {
        // used to ignore any unknown JSON property
        // so that we can load DTO from compound JSON objects
        // i.e. using #createDto(Map<String, String> data)
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PrintPressBaseDto getPrintPress() {
        return printPress;
    }

    public void setPrintPress(PrintPressBaseDto printPress) {
        this.printPress = printPress;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
