package com.letak.printing.domain_model.dto.order;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.letak.printing.domain_model.dto.attribute.ProductAttributeBaseDto;
import com.letak.printing.domain_model.dto.attribute.ProductAttributeValueBaseDto;
import com.letak.printing.domain_model.entity.ProductAttribute;
import com.letak.printing.domain_model.entity.ProductAttributeValue;

public class AttributeSelection {

    private final ProductAttributeBaseDto attribute;
    private final ProductAttributeValueBaseDto value;

    public AttributeSelection(ProductAttribute attribute, ProductAttributeValue value) {
        this.attribute = new ProductAttributeBaseDto(attribute);
        this.value = new ProductAttributeValueBaseDto(value);
    }

    public static String toJson(List<AttributeSelection> data) throws JsonProcessingException {
        
        ObjectMapper mapper = new ObjectMapper();
        mapper.setDefaultPropertyInclusion(Include.NON_NULL);
        return mapper.writeValueAsString(data);
    }

    public static List<AttributeSelection>  fromJson(String content) throws IOException {
        
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<List<AttributeSelection>> valueTypeRef = new TypeReference<List<AttributeSelection>>() {
        };
        return mapper.readValue(content, valueTypeRef);
    }

    public static List<AttributeSelection> sanitize(List<AttributeSelection> attrValuesList) {
        
        for(AttributeSelection sel : attrValuesList) {
            sel.getAttribute().setProduct(null);
            sel.getAttribute().setScriptName(null);
            sel.getAttribute().setType(null);
            
            sel.getValue().setProductAttribute(null);
            sel.getValue().setScriptValue(null);
        }

        return attrValuesList;
    }

    public ProductAttributeBaseDto getAttribute() {
        return attribute;
    }

    public ProductAttributeValueBaseDto getValue() {
        return value;
    }

}
