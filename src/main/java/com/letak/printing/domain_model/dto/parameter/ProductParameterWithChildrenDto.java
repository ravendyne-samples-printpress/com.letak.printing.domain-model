package com.letak.printing.domain_model.dto.parameter;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.entity.ProductParameter;
import com.letak.printing.domain_model.entity.ProductParameterValue;

public class ProductParameterWithChildrenDto extends ProductParameterBaseDto {

    private List<ProductParameterValueBaseDto> productParameterValues;
    
    public ProductParameterWithChildrenDto() {
    }
    
    public ProductParameterWithChildrenDto(ProductParameter productParameter) {
        super(productParameter);
        
        productParameterValues = new ArrayList<>();
        
        for(ProductParameterValue value : productParameter.getProductParameterValues()) {
            productParameterValues.add(new ProductParameterValueBaseDto(value));
        }
    }
    
    public static ProductParameter createEntity(ProductParameterWithChildrenDto dto) {
        ProductParameter entity = ProductParameterBaseDto.createEntity(dto);
        
        List<ProductParameterValue> productParameterValuesList = new ArrayList<>();
        entity.setProductParameterValues(productParameterValuesList);
        
        for(ProductParameterValueBaseDto ppv: dto.getProductParameterValues()) {
            productParameterValuesList.add(ProductParameterValueBaseDto.createEntity(ppv));
        }
        
        return entity;
    }

    public List<ProductParameterValueBaseDto> getProductParameterValues() {
        return productParameterValues;
    }

    public void setProductParameterValues(List<ProductParameterValueBaseDto> productParameterValues) {
        this.productParameterValues = productParameterValues;
    }

}
