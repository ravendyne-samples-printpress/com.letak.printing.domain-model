package com.letak.printing.domain_model.dto.printpress;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.dto.product.ProductBaseDto;
import com.letak.printing.domain_model.entity.PrintPress;
import com.letak.printing.domain_model.entity.Product;

public class PrintPressWithBaseChildrenDto extends PrintPressBaseDto {

    private List<ProductBaseDto> products;

    public PrintPressWithBaseChildrenDto() {
    }

    public PrintPressWithBaseChildrenDto(PrintPress printPress) {
        super(printPress);
        
        products = new ArrayList<>();
        for(Product product : printPress.getProducts()) {
            products.add(new ProductBaseDto(product));
        }
    }
    
    public static PrintPress createEntity(PrintPressWithBaseChildrenDto dto) {
        PrintPress entity = PrintPressBaseDto.createEntity(dto);
        
        List<Product> productsList = new ArrayList<>();
        for(ProductBaseDto prod : dto.getProducts()) {
            productsList.add(ProductBaseDto.createEntity(prod));
        }
        entity.setProducts(productsList);

        return entity;
    }

    public List<ProductBaseDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductBaseDto> products) {
        this.products = products;
    }
}
