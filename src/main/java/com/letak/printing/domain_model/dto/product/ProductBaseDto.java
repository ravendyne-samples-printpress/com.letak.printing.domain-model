package com.letak.printing.domain_model.dto.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.letak.printing.domain_model.dto.printpress.PrintPressBaseDto;
import com.letak.printing.domain_model.entity.Product;

public class ProductBaseDto {

    private Integer id;

    private String name;

    private String icon;

    private Integer displayOrder;
    
    private String description;

    @JsonProperty(access = Access.WRITE_ONLY)
    private PrintPressBaseDto printPress;
    
    public ProductBaseDto() {}
    
    public ProductBaseDto(Product product) {
        setId(product.getId());
        setName(product.getName());
        setIcon(product.getIcon());
        setDisplayOrder(product.getDisplayOrder());
        setDescription(product.getDescription());
        
//        setPrintPress(new PrintPressBaseDto(product.getPrintPress()));
    }

    public static Product createEntity(ProductBaseDto prod) {
        Product entity = new Product();

        entity.setId(prod.getId());
        entity.setName(prod.getName());
        entity.setIcon(prod.getIcon());
        entity.setDisplayOrder(prod.getDisplayOrder());
        entity.setDescription(prod.getDescription());

        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PrintPressBaseDto getPrintPress() {
        return printPress;
    }

    public void setPrintPress(PrintPressBaseDto printPress) {
        this.printPress = printPress;
    }
}
