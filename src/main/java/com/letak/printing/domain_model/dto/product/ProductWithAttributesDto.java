package com.letak.printing.domain_model.dto.product;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.dto.attribute.ProductAttributeWithChildrenDto;
import com.letak.printing.domain_model.entity.Product;
import com.letak.printing.domain_model.entity.ProductAttribute;

public class ProductWithAttributesDto extends ProductBaseDto {

    private List<ProductAttributeWithChildrenDto> productAttributes;
    
    public ProductWithAttributesDto() {}
    
    public ProductWithAttributesDto(Product product) {
        super(product);
        
        productAttributes = new ArrayList<>();
        for(ProductAttribute attr : product.getProductAttributes()){
            productAttributes.add(new ProductAttributeWithChildrenDto(attr));
        }
    }
    
    public static Product createEntity(ProductWithAttributesDto dto) {
        Product entity = ProductBaseDto.createEntity(dto);
        
        List<ProductAttribute> productAttributesList = new ArrayList<>();
        
        for(ProductAttributeWithChildrenDto attr : dto.getProductAttributes()) {
            ProductAttribute pa = ProductAttributeWithChildrenDto.createEntity(attr);
            productAttributesList.add(pa);
        }
        
        entity.setProductAttributes(productAttributesList);
        
        return entity;
    }

    public List<ProductAttributeWithChildrenDto> getProductAttributes() {
        return productAttributes;
    }

    public void setProductAttributes(List<ProductAttributeWithChildrenDto> productAttributes) {
        this.productAttributes = productAttributes;
    }
}
