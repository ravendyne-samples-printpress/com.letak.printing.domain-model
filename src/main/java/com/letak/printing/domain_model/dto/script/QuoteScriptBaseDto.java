package com.letak.printing.domain_model.dto.script;

import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.letak.printing.domain_model.dto.product.ProductBaseDto;
import com.letak.printing.domain_model.entity.QuoteScript;

public class QuoteScriptBaseDto {

    private Integer id;

    private String name;

    private Integer scriptOrder;

    private List<String> source;

    @JsonProperty(access = Access.WRITE_ONLY)
    private ProductBaseDto product;

    public QuoteScriptBaseDto() {
    }

    public QuoteScriptBaseDto(QuoteScript quoteScript) {
        setId(quoteScript.getId());
        setName(quoteScript.getName());
        setScriptOrder(quoteScript.getScriptOrder());
        setScriptSource(quoteScript.getScriptSource());
        
//        setProduct(new ProductBaseDto(quoteScript.getProduct()));
    }
    
    public static QuoteScript createEntity(QuoteScriptBaseDto dto) {
        QuoteScript entity = new QuoteScript();
        
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setScriptOrder(dto.getScriptOrder());
        entity.setScriptSource(dto.getScriptSource());
        
        return entity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScriptOrder() {
        return scriptOrder;
    }

    public void setScriptOrder(Integer scriptOrder) {
        this.scriptOrder = scriptOrder;
    }

    public ProductBaseDto getProduct() {
        return product;
    }

    public void setProduct(ProductBaseDto product) {
        this.product = product;
    }

    public List<String> getSource() {
        return source;
    }

    public void setSource(List<String> source) {
        this.source = source;
    }

    @JsonIgnore
    public String getScriptSource() {
        return String.join("\n", source);
    }

    public void setScriptSource(String scriptSource) {
        this.source = Arrays.asList(scriptSource.split("\n"));
    }
}
