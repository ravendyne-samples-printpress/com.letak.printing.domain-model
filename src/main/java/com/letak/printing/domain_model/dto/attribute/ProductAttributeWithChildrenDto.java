package com.letak.printing.domain_model.dto.attribute;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.entity.ProductAttribute;
import com.letak.printing.domain_model.entity.ProductAttributeValue;

public class ProductAttributeWithChildrenDto extends ProductAttributeBaseDto {

    private List<ProductAttributeValueBaseDto> productAttributeValues;

    public ProductAttributeWithChildrenDto() {}

    public ProductAttributeWithChildrenDto(ProductAttribute pa) {
        super(pa);
        
        productAttributeValues = new ArrayList<>();
        
        for(ProductAttributeValue value : pa.getProductAttributeValues()) {
            productAttributeValues.add(new ProductAttributeValueBaseDto(value));
        }
    }
    
    public static ProductAttribute createEntity(ProductAttributeWithChildrenDto dto) {
        ProductAttribute entity = ProductAttributeBaseDto.createEntity(dto);
        
        List<ProductAttributeValue> productAttributeValuesList = new ArrayList<>();
        for(ProductAttributeValueBaseDto pav : dto.getProductAttributeValues()) {
            productAttributeValuesList.add(ProductAttributeValueBaseDto.createEntity(pav));
        }
        
        entity.setProductAttributeValues(productAttributeValuesList);
        
        return entity;
    }

    public List<ProductAttributeValueBaseDto> getProductAttributeValues() {
        return productAttributeValues;
    }

    public void setProductAttributeValues(List<ProductAttributeValueBaseDto> productAttributeValues) {
        this.productAttributeValues = productAttributeValues;
    }
}
