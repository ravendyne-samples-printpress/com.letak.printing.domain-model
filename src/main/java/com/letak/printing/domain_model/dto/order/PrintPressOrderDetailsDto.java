package com.letak.printing.domain_model.dto.order;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.letak.printing.domain_model.entity.PrintPressOrder;

public class PrintPressOrderDetailsDto extends PrintPressOrderBaseDto {

    private String urlSlug;

    private List<AttributeSelection> attributeSelections;

    public PrintPressOrderDetailsDto() {}
    
    public PrintPressOrderDetailsDto(PrintPressOrder order) throws IOException {
        super(order);

        setUrlSlug(order.getUrlSlug());
        setAttributeSelections(AttributeSelection.fromJson(order.getDetailsJson()));
    }
    
    public static PrintPressOrder createEntity(PrintPressOrderDetailsDto dto) throws JsonProcessingException {
        PrintPressOrder entity = PrintPressOrderBaseDto.createEntity(dto);
        
        entity.setUrlSlug(dto.getUrlSlug());
        entity.setDetailsJson(AttributeSelection.toJson(dto.getAttributeSelections()));
        
        return entity;
    }
    
    public static PrintPressOrderDetailsDto createDto(Map<String, String> data) throws JsonProcessingException {

        ObjectMapper mapper = new ObjectMapper();

        PrintPressOrderDetailsDto dto = mapper.convertValue(data, PrintPressOrderDetailsDto.class);
        PrintPressOrderDetailsDto.createEntity(dto);

        return dto;
    }
    
    @JsonAnySetter
    public void catchAny(String name, String value) {
        // used to ignore any unknown JSON property
        // so that we can load DTO from compound JSON objects
        // i.e. using #createDto(Map<String, String> data)
    }

    public String getUrlSlug() {
        return urlSlug;
    }

    public void setUrlSlug(String urlSlug) {
        this.urlSlug = urlSlug;
    }

    public List<AttributeSelection> getAttributeSelections() {
        return attributeSelections;
    }

    public void setAttributeSelections(List<AttributeSelection> attributeSelections) {
        this.attributeSelections = attributeSelections;
    }
}
