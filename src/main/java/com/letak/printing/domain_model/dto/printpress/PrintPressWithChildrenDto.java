package com.letak.printing.domain_model.dto.printpress;

import java.util.ArrayList;
import java.util.List;

import com.letak.printing.domain_model.dto.product.ProductWithChildrenDto;
import com.letak.printing.domain_model.entity.PrintPress;
import com.letak.printing.domain_model.entity.Product;

public class PrintPressWithChildrenDto extends PrintPressBaseDto {

    private List<ProductWithChildrenDto> products;

    public PrintPressWithChildrenDto() {
    }

    public PrintPressWithChildrenDto(PrintPress printPress) {
        super(printPress);
        
        products = new ArrayList<>();
        for(Product product : printPress.getProducts()) {
            products.add(new ProductWithChildrenDto(product));
        }
    }
    
    public static PrintPress createEntity(PrintPressWithChildrenDto dto) {
        PrintPress entity = PrintPressBaseDto.createEntity(dto);
        
        List<Product> productsList = new ArrayList<>();
        for(ProductWithChildrenDto prod : dto.getProducts()) {
            productsList.add(ProductWithChildrenDto.createEntity(prod));
        }
        entity.setProducts(productsList);

        return entity;
    }

    public List<ProductWithChildrenDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductWithChildrenDto> products) {
        this.products = products;
    }
}
