package com.letak.printing.domain_model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class ProductParameter implements Serializable {

    private static final long serialVersionUID = 8302072054352064129L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(nullable = false)
    protected String name;

    @Column(/*nullable = false*/)
    protected String scriptName;

    @Column(/*nullable = false*/)
    protected String defaultValue;

    @Column(/*nullable = false*/)
    protected String type;

    @JsonProperty(access = Access.WRITE_ONLY)
    @ManyToOne(optional = false)
    protected Product product;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT) // use with fetch = FetchType.EAGER, Hibernate specific
    @JoinColumn(name = "PRODUCT_PARAMETER_ID")
    private List<ProductParameterValue> productParameterValues;

    public ProductParameter() {}

    public ProductParameter(Integer id) {
        this.id = id;
    }

    public List<ProductParameterValue> getProductParameterValues() {
        return productParameterValues;
    }

    public void setProductParameterValues(List<ProductParameterValue> productParameterValues) {
        this.productParameterValues = productParameterValues;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScriptName() {
        return scriptName;
    }

    public void setScriptName(String scriptName) {
        this.scriptName = scriptName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
}
