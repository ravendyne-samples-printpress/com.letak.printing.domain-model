package com.letak.printing.domain_model.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = 4581961609128976664L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(nullable = false)
    protected String name;

    @Column
    protected String icon;

    @Column
    protected Integer displayOrder;
    
    @Lob
    @Column(nullable = false, length = 10000)
    protected String description;

    @JsonProperty(access = Access.WRITE_ONLY)
    @ManyToOne(optional = false)
    protected PrintPress printPress;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT) // use with fetch = FetchType.EAGER, Hibernate specific
//    @LazyCollection(LazyCollectionOption.FALSE) // use without fetch = FetchType.EAGER, Hibernate specific
    // @JoinColumn will drop the auto-creation of join table for @OneToMany
    @JoinColumn(name = "PRODUCT_ID")
    private List<ProductAttribute> productAttributes;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT) // use with fetch = FetchType.EAGER, Hibernate specific
    // @JoinColumn will drop the auto-creation of join table for @OneToMany
    @JoinColumn(name = "PRODUCT_ID")
    private List<ProductParameter> productParameters;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT) // use with fetch = FetchType.EAGER, Hibernate specific
    // @JoinColumn will drop the auto-creation of join table for @OneToMany
    @JoinColumn(name = "PRODUCT_ID")
    private List<QuoteScript> productScripts;

    public Product() {}

    public Product(Integer id) {
        this.id = id;
    }

    public List<ProductParameter> getProductParameters() {
        return productParameters;
    }

    public void setProductParameters(List<ProductParameter> productParameters) {
        this.productParameters = productParameters;
    }

    public List<QuoteScript> getProductScripts() {
        return productScripts;
    }

    public void setProductScripts(List<QuoteScript> productScripts) {
        this.productScripts = productScripts;
    }

    public List<ProductAttribute> getProductAttributes() {
        return productAttributes;
    }

    public void setProductAttributes(List<ProductAttribute> productAttributes) {
        this.productAttributes = productAttributes;
    }

    public PrintPress getPrintPress() {
        return printPress;
    }

    public void setPrintPress(PrintPress printPress) {
        this.printPress = printPress;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Integer displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
