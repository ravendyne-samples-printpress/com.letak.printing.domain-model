package com.letak.printing.domain_model.entity;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
public class QuoteScript implements Serializable {
    
    private static final long serialVersionUID = 2284202384222184252L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(nullable = false)
    protected String name;

    @Column(nullable = false)
    protected Integer scriptOrder;

    @JsonIgnore
    @Lob
    @Column(nullable = false, length=10000)
    protected String scriptSource;

    @JsonProperty(access = Access.WRITE_ONLY)
    @ManyToOne(optional = false)
    protected Product product;

    public QuoteScript() {}
    
    public QuoteScript(Integer id) {
        this.id = id;
    }

    // TODO remove this, DTO is taking care of it now
    // These two methods are intended for Jackson
    // so we can store script source as an array of strings
    // in JSON files (JSON does not support multiline strings]
    @Deprecated
    public List<String> getSource() {
        return Arrays.asList(scriptSource.split("\n"));
    }

    @Deprecated
    public void setSource(List<String> source) {
        this.scriptSource = String.join("\n", source);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getScriptOrder() {
        return scriptOrder;
    }

    public void setScriptOrder(Integer scriptOrder) {
        this.scriptOrder = scriptOrder;
    }

    public String getScriptSource() {
        return scriptSource;
    }

    public void setScriptSource(String scriptSource) {
        this.scriptSource = scriptSource;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

}
