## PrintPress Domain Model Package

Provides single place where domain model POJOs are defined. Other projects reference this one via their `pom.xml`s and then subclass if needed.
